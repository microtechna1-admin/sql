package jdbcExample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLExample {

	public static void main(String[] args) throws SQLException {
		Connection conn1 = null;
		Statement stmt = null;
		
		String url = "jdbc:mysql://localhost:3306/dbmicrotech";
		String user = "root";
		String password = "root";
		
		try {
			conn1 = DriverManager.getConnection(url, user, password);
			//if(conn1 != null) {
				System.out.println("connected");
				System.out.println(conn1);
			//}
			stmt = conn1.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT prodId, prodName, prodPrice FROM tblProduct");
			
			System.out.println("Product Id\tProduct Name\tPorduct Price");
			System.out.println("--------------------------------------------");
			while(rs.next()){
				int pId = rs.getInt("prodId");
				String pName = rs.getString("prodName");
				double pPrice = rs.getDouble("prodPrice");
				System.out.println(pId+ "\t\t"+pName+ "\t\t"+pPrice+ "\t\t");
			}
		} 
		catch (SQLException e) {
			System.out.println("not found");
			System.out.println("Wrong Credential");
		}
		finally{
			if(conn1 != null);
				conn1.close();
		}

	}

}
